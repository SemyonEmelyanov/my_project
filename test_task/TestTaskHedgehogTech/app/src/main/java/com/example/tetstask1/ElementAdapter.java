package com.example.tetstask1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElementAdapter extends RecyclerView.Adapter <ElementAdapter.ElementHolder>{
    private List<Element> elementList = new ArrayList<Element>();
    private Map <String,String> mapJok = new HashMap<String,String>();
    @NonNull
    @Override
    public ElementHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewResult = LayoutInflater.from(parent.getContext()).inflate(R.layout.element,parent,false);
        return new ElementHolder(viewResult);
    }

    @Override
    public void onBindViewHolder(@NonNull ElementHolder holder, int position) {
        holder.elementTextView = holder.itemView.findViewById(R.id.text_title);
        holder.elementTextView.setText(elementList.get(position).getElementSTR());
    }
    @Override
    public int getItemCount() {
        return elementList.size();
    }
    public void addElementList(Element element){
        elementList.add(element);
        notifyDataSetChanged();
    }

    public void Clear (){
        elementList.clear();
        mapJok.clear();
        notifyDataSetChanged();
    }

    public void setMapJok(Map<String,String> mapJok){
        this.mapJok = mapJok;
    }


    public class ElementHolder extends RecyclerView.ViewHolder  implements View.OnClickListener  {
        private TextView elementTextView;
        private boolean clicForMe = true;
        private String numberJoke = null;
        public ElementHolder(@NonNull View itemView) {
            super(itemView);
            elementTextView = itemView.findViewById(R.id.text_title);
            elementTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clicForMe) {
                numberJoke = (String) elementTextView.getText();
                elementTextView.setText(mapJok.get(elementTextView.getText()));
                clicForMe = false;
            }
            else {
                elementTextView.setText(numberJoke);
                clicForMe = true;
            }
        }
    }

}
