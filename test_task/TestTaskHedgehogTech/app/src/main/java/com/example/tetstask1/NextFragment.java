package com.example.tetstask1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toolbar;

import androidx.fragment.app.Fragment;

public class NextFragment extends Fragment {
    private WebView webView;
    public NextFragment() {
    }
    private String webURL = "https://www.icndb.com/api/";
    private String saveWebURL = "URL";
    public static NextFragment newInstance() {
        return new NextFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewResult =inflater.inflate(R.layout.fragment_next, container, false);
        webView = (WebView) viewResult.findViewById(R.id.web_view);
        webView.loadUrl("https://www.icndb.com/api/");//https://www.icndb.com/api/
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        getActivity().setTitle("Web");
        return viewResult;
    }
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(saveWebURL,webView.getUrl().toString());
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {

        } else {
            webView.loadUrl(savedInstanceState.getString(saveWebURL));
        }
    }
}
