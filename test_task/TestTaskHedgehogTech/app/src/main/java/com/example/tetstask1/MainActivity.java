package com.example.tetstask1;

import static com.example.tetstask1.NetworkUtils.generateURL;
import static com.example.tetstask1.NetworkUtils.getResponseFromURL;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private int count = 0;
    private EditText edCount;
    private Fragment thisFragment;
    private String ARG_TITLE = "Title";
    private String ARG_LISTJOKS = "MapJoks";
    private String ARG_TITLE_NAME = "TitleName";
    private Map<String,String> mapJoks = new HashMap<String,String>();
    private List <String> listJoke = new ArrayList<String>();
    private URL generateURL;
    private RecyclerView rcView;
    private ElementAdapter elementAdapter = new ElementAdapter();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(thisFragment = HomeFragment.newInstance());
                    return true;
                case R.id.navigation_next:
                    loadFragment(thisFragment = NextFragment.newInstance());
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fl_content, fragment);
        ft.commit();
    }
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        if (savedInstanceState==null){
            fragmentTransaction.add(R.id.fl_content,thisFragment = HomeFragment.newInstance()).commit();
            this.setTitle("Jokes");
        }
        else {
            fragmentTransaction.replace(R.id.fl_content,thisFragment=fragmentManager.getFragment(savedInstanceState,ARG_TITLE)).commit();
            this.setTitle(savedInstanceState.getString(ARG_TITLE_NAME));
        }
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.getString(ARG_TITLE_NAME)=="Jokes"){
            elementAdapter.Clear();
            listJoke = savedInstanceState.getStringArrayList(ARG_LISTJOKS);
            count = listJoke.size();
            rcView = findViewById(R.id.jokes_grid);
            rcView.setLayoutManager(new GridLayoutManager(rcView.getContext(),1));
            rcView.setAdapter(elementAdapter);
            for (int index = 1; index<(count+1);index++) {
                mapJoks.put("Joke" + index ,listJoke.get(index-1));
                elementAdapter.addElementList(new Element("Joke" + index, index));
            }
            elementAdapter.setMapJok(mapJoks);
            rcView.setAdapter(elementAdapter);


        }
        Log.d("Inet", "onRestoreInstanceState");
    }

    public void clickMyBtn(View view) {
        edCount = findViewById(R.id.count);
        if (!edCount.getText().toString().isEmpty()){
            elementAdapter.Clear();
            mapJoks.clear();
            listJoke.clear();
            count = (edCount.getText() == null) ? 0 : Integer.parseInt(edCount.getText().toString());
            generateURL = generateURL(Integer.toString(count));
            new InternetQueryTask().execute(generateURL);
            rcView = findViewById(R.id.jokes_grid);
            rcView.setLayoutManager(new GridLayoutManager(rcView.getContext(),1));
            rcView.setAdapter(elementAdapter);
            for (int index = 1; index<(count+1);index++) {
                elementAdapter.addElementList(new Element("Joke" + index, index));
            }
            elementAdapter.setMapJok(mapJoks);
            rcView.setAdapter(elementAdapter);
            edCount.setText("");
        }
        else count = 0;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentManager.putFragment(outState,ARG_TITLE,thisFragment);
        outState.putString(ARG_TITLE_NAME,this.getTitle().toString());
        outState.putStringArrayList(ARG_LISTJOKS, (ArrayList<String>) listJoke);
    }

    class InternetQueryTask extends AsyncTask <URL,Void,String>{
        @Override
        protected String doInBackground(URL... urls) {
            String response = null;
            try{
                response = getResponseFromURL(urls[0]);
            }catch (IOException e){
                e.printStackTrace();
            }
            return response;
        }
        private void showResult(){
            RecyclerView rcView = findViewById(R.id.jokes_grid);
            TextView textView = findViewById(R.id.message_error);
            textView.setVisibility(View.INVISIBLE);
        }
        private void showErrorTextView(){
            RecyclerView rcView = findViewById(R.id.jokes_grid);
            TextView textView = findViewById(R.id.message_error);
            textView.setVisibility(View.VISIBLE);
            rcView.setVisibility(View.INVISIBLE);
        }
        @Override
        protected void onPostExecute(String response) {
            String jokeValue = null;
            if (response != null && response != "") {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray jsonArray = jsonResponse.getJSONArray("value");
                    for (int index = 0; index<count;index++){
                        JSONObject jokeInfo = jsonArray.getJSONObject(index);
                        jokeValue = jokeInfo.getString("joke");
                        mapJoks.put("Joke" + (index+1) ,jokeValue);
                        listJoke.add(jokeValue);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showResult();
            }else {
                showErrorTextView();
            }
        }
    }
    public Map <String,String> getMapJoks (){
        return mapJoks;
    }

}