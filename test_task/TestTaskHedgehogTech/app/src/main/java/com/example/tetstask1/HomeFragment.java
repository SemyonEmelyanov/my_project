package com.example.tetstask1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeFragment extends Fragment {
    private String countString;
    private String ARG_COUNT = "countFragment";
    private String ARG_LISP = "elementsLisp";
    public EditText countEditText;
    private RecyclerView rcView;
    private Button addButton;
    private EditText edCount;
    private int count;
    private ElementAdapter elementAdapter = new ElementAdapter();
    public HomeFragment() {
    }
    public static HomeFragment newInstance() {
        return new HomeFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.fragment_home,container,false);
        countEditText = viewFragment.findViewById(R.id.count);
        getActivity().setTitle("Jokes");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_COUNT,countEditText.getText().toString());
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {

        } else {
            countEditText.setText(savedInstanceState.getString(ARG_COUNT));
        }
    }

}
