#include "player.h"

void Player::Startpos(Field* area){
    x_pos = area->getXpos();
	y_pos = area->getYpos();	
}

Player::Player(){
        apple = 0;
        tool = 0;
        trap= 0;
}

int Player::Kbhit(){
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}

void Player::Update(Field* area){
    if (this->Kbhit()!=0){
        char symbol = fgetc(stdin);
        switch (symbol) {
            case 'w': 
                if(area->CellInformationOpen(x_pos,y_pos-1)) y_pos--;
                break;
            case 'a': 
                if(area->CellInformationOpen(x_pos-1,y_pos)) x_pos--;
                break;
            case 's': 
                if(area->CellInformationOpen(x_pos,y_pos+1)) y_pos++;
                break;
            case 'd': 
                if(area->CellInformationOpen(x_pos+1,y_pos)) x_pos++;
                break;
            case 'q':
                exit(1);
                break;
        }
    }
}

void Player::nonblock(int state) {
  struct termios ttystate;

  // Get the terminal state.
  tcgetattr(STDIN_FILENO, &ttystate);

  if (state == NB_ENABLE) {
    // Turn off canonical mode.
    ttystate.c_lflag &= ~ICANON;
    // Minimum of number input read.
    ttystate.c_cc[VMIN] = 1;
  } else if (state == NB_DISABLE) {
    // Turn on canonical mode.
    ttystate.c_lflag |= ICANON;
  }
  // Set the terminal attributes.
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

bool Player::Playerpos(int x, int y){
    if ((x == x_pos)&&(y == y_pos)) return true;
    else return false;
}

void Player::CoutPlayerInformation(){
    std::cout<<"Игрок находится: "<<x_pos<<' '<<y_pos<<std::endl;
    std::cout<<"Очков: "<<apple<<"/7"<<std::endl;
    std::cout<<"Ловушек: "<<trap<<"/3"<<std::endl;
    std::cout<<"Инструментов: "<<tool<<"/2"<<std::endl;     
}

int Player::GetCoordx(){
    return this->x_pos;
}
int Player::GetCoordy(){
    return this->y_pos;
}

void Player::operator<<(Object* object){
    if(object->getName()=='o') this-> apple++;
    if(object->getName()=='|') if (this->tool+this->trap<this->apple)this-> tool++;
    if(object->getName()=='*'){
        if ((this->tool!=0)&&(this->tool+this->trap<this->apple)){
            this->trap++;
            this->tool--;
        }
    }
}

bool Player::HappyPlayer(){
    if(this->trap==3&&this->apple==7){
        return true;
    }
    else{
        return false;
    }
}

int Player::GetApple(){
    return this->apple;
}
int Player::GetTrap(){
    return this->trap;
}
int Player::GetTool(){
    return this->tool;
}