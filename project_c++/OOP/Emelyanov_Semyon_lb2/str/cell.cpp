#include "cell.h"


Cell::Cell(){
    object = new ObjectCellEmpty;
}

Cell::~Cell(){}

bool Cell::isOpen(){
    if (object->getName() == ' '||object->getName() == 'T'||object->getName() == 'o'||object->getName() == '|'||object->getName() == '*'||object->getName() == 'E'){
        return true;
    }
    else{
        return false;
    }
}

std::string Cell::Information(){
    switch(object->getName()){
        case ' ':
            return "\x1b[47m \x1b[0m";
        case '#':
            return "\x1b[47;30;1m#\x1b[0m";
        case 'o':
            return "\x1b[47;31mo\x1b[0m";
        case '|':
            return "\x1b[47;36m|\x1b[0m";
        case '*':
            return "\x1b[47;30m*\x1b[0m";
        case 'W':
            return "\x1b[47;36mW\x1b[0m";
        case 'E':
            return "\x1b[42;31;1mE\x1b[0m";
        case 'S':
            return "@";
        case '@':
            return "@";
    }
}
