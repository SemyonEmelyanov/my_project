#pragma once 
#include "object.h" 

class Cell{
public:
	Cell();
	~Cell();
	Object* object; 
	bool isOpen();
	std::string Information();
};