#include "player.h"
#include <fstream>
#pragma once 
#include "strategy.h"
#define Sleep(x) usleep(x*2000)
#define INTERVAL 200

class Game {

    public: 
        void StartGame();
        void GameOver();
        void GameShow(Field* area, Player* player);   
        void GameLogic(Field* area, Player* player,Context*);
};