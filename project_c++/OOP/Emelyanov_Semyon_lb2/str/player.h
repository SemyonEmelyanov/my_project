#pragma once 
#include <iostream>
#include "field.h"
#define STDIN_FILENO 0
#define NB_DISABLE 0
#define NB_ENABLE 1

class Player{
    private: 
        int x_pos;
        int y_pos;
        int apple;  
        int tool;
        int trap;       
    public:
        Player();
        void Update(Field* area);
        void Startpos(Field* area);
        int Kbhit();
        void nonblock(int state);
        bool Playerpos(int x, int y);
        void CoutPlayerInformation();
        int GetCoordx();
        int GetCoordy();
        void operator<<(Object* object);
        bool HappyPlayer();
        int GetApple();
        int GetTrap();
        int GetTool();
};