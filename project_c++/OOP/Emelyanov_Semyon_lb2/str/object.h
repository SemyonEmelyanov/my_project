#pragma once 
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <termios.h>
#include <sys/select.h>
#include <memory.h>


class Object{
    public:
        virtual char const getName() = 0;
};
// Объекты 
class ObjectBusy: public Object{
    public:
        char const getName();
};

class ObjectCellExit: public Object{
    public:
        char const getName();
};

class  ObjectCellEmpty: public Object{
    public:
        char const getName();
};

class ObjectTrap: public Object{
    public:
        char const getName();
};

class ObjectApple: public Object{
    public:
        char const getName();
};

class ObjectWeapon: public Object{
    public:
        char const getName();
};

class ObjectTool: public Object{
    public:
        char const getName();
};

//


class FactoryObject{
    public:
        virtual Object* createObject() = 0;
        //virtual Object* createObject(int x, int y) = 0;
};
//Конструкторы объектов
class FactoryObjectBusy: public FactoryObject {
    public:
        Object* createObject();
       // Object* createObject(int x, int y);
};

class FactoryObjectCellExit: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};

class FactoryObjectCellEmpty: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};

class FactoryObjectTrap: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};

class FactoryObjectApple: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};

class FactoryObjectWeapon: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};

class FactoryObjectTool: public FactoryObject {
    public:
        Object* createObject();
        //Object* createObject(int x, int y);
};
//