#pragma once 
#include"player.h"

class Strategy {

public:
    //virtual ~Strategy() {}
    virtual void doStrategy(Player*,Field*) = 0;
};

class Context {

private:
    Strategy* strategy;

public:
    Context(Strategy* strategy = nullptr) : strategy(strategy) {}
    ~Context();

    void SetStrategy(Strategy* strategy);
    void DoSomeBusinessLogic(Player* player, Field* area);
};

class Strategy1 : public Strategy {

public:
    void doStrategy(Player*, Field* area );
};

class Strategy2 : public Strategy {

public:
    void doStrategy(Player*, Field* area);
};