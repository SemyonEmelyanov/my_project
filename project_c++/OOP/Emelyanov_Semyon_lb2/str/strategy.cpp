#include "strategy.h"
Context::~Context() {

	delete this->strategy;
}

void Context::SetStrategy(Strategy* strategy) {

    delete this->strategy;
    this->strategy = strategy;
}

void Context::DoSomeBusinessLogic(Player* player, Field* area) {

    this->strategy->doStrategy(player,area);
}

void Strategy1::doStrategy(Player* player, Field* area) {

    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='o'){
        *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
        area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
    }
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='|'){
        if (player->GetTool()+player->GetTrap()<player->GetApple()){
            *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
            area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
        }

    }
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='*'){
        if ((player->GetTool()!=0)&&(player->GetTool()+player->GetTrap()<player->GetApple())){
            *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
            area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
        }
    }
}

void Strategy2::doStrategy(Player* player, Field* area) {
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='E'){
        std::cout<<'\n'<<"\x1b[43;31mПоздравляю Вы выиграли!!!\x1b[0m"<<std::endl;
        exit(1);
    }
    
}