#include "comand.h"


void CommandStartGame::execute(Game* game){
    game->StartGame();
}

void CommandEndGame::execute(Game* game){
    game->EndGame();
}

void CommandMovePlayer::execute(Game* game){
    game->MovePlayer(this->symbol);
}