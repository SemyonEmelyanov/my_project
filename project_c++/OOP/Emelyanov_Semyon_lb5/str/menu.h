#pragma once
#include "game.h"
#include "comand.h"
#define Sleep(x) usleep(x*2000)
#define INTERVAL 200

class Menu{
    CommandStartGame* csg = new CommandStartGame;
    CommandEndGame* ceg = new CommandEndGame;
    CommandMovePlayer* cmp = new CommandMovePlayer;
    public:
        ~Menu();
        void menu();
        int Kbhit();
        void nonblock(int state);
};
