#pragma once 
#include "player.h"
#include <fstream>
#include "strategy.h"
#include "enemy.h"
#include "state.h"
#define Sleep(x) usleep(x*2000)
#define INTERVAL 200

class Game {
    CellObserver* cell_observer;
	Abstraction* abstraction;
    Field* area;
    Player* player;
    Context* context;
    int CountEnemy = 3;
    int CountStatus = 0;
    Enemy<type>** enemy = new Enemy<type>*[CountEnemy];
    State* state ;//= new StadingState(enemy, CountStatus);
    void GameShow(Field* area, Player* player);   
    void GameLogic(Field* area, Player* player,Context*);
    std::string StatusState(){
        if(state->getTypeState() == 0){
            return "Сейчас враги спят!\n";
        }
        if(state->getTypeState() == 1){
            return "Сейчас враги активны!\n";
        }
        if(state->getTypeState() == 2){
            return "Сейчас враги в яростном режиме!\n";
        }

    }
    public:
        ~Game();
        void StartGame();
        void MovePlayer(char symbol);
        //void GameOver();
        void EndGame();
        void GamePlay();
        Player* GetPlayer();
        void SetStatusEnemy(){
            if(CountStatus == 0){
                delete this->state;
                this->state = new StadingState(enemy, CountEnemy);
            }
            if(CountStatus == 10){
                delete this->state;
                this->state = new RunState(enemy, CountEnemy);
            }
            if(CountStatus == 30){
                delete this->state;
                this->state = new RageState(enemy, CountEnemy);
                CountStatus = -10;
            }

        }
        void EnemyMove(){
            for(int i = 0; i < CountEnemy; i++){
                enemy[i]->update();
                *enemy[i]<<player;
            }
            CountStatus++;
        }
};