#pragma once 
#include <fstream>


class Publisher;
class AbstractObserver{
public:
    virtual void update(Publisher* unit) = 0;
};

class Abstraction{
    protected: 
        AbstractObserver* AbstractionObserver;
    public: 
        Abstraction(AbstractObserver* AbstractionObserver){
            this->AbstractionObserver = AbstractionObserver;
        }
        virtual void Operation(Publisher* unit) const{
            this->AbstractionObserver->update(unit);
        }
        virtual ~Abstraction(){};
};      