#pragma once 
#include "game.h"

class AbstractCommand{
    public:
        virtual void execute(Game* game) = 0;

};
//---------------------------
class CommandStartGame:public AbstractCommand{ //:public AbstractCommand{
    public:
        void execute(Game* game)override;
};

class CommandEndGame:public AbstractCommand{
    public:
        void execute(Game* game)override;

};

class CommandMovePlayer:public AbstractCommand{
    public:
        char symbol;
        void execute(Game* game)override;

};