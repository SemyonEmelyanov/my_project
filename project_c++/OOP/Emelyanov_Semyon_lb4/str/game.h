#pragma once 
#include "player.h"
#include <fstream>
#include "strategy.h"
#define Sleep(x) usleep(x*2000)
#define INTERVAL 200

class Game {
    CellObserver* cell_observer;
	Abstraction* abstraction;
    Field* area;
    Player* player;
    Context* context;
    void GameShow(Field* area, Player* player);   
    void GameLogic(Field* area, Player* player,Context*);
    public:
        ~Game();
        void StartGame();
        void MovePlayer(char symbol);
        //void GameOver();
        void EndGame();
        void GamePlay();
        Player* GetPlayer();
};