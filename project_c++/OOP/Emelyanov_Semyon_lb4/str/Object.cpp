#include "object.h" 

//ObjectBusy
char const ObjectBusy::getName(){
    return '#';
}
Object* FactoryObjectBusy::createObject(){
    return new ObjectBusy;
}

//---------------------------------------------------

//ObjectCellEmpty
char const ObjectCellEmpty::getName(){
    return ' ';
}

Object* FactoryObjectCellEmpty::createObject(){
    return new ObjectCellEmpty;
}

//-----------------------------------------------------

//ObjectCellExit
char const ObjectCellExit::getName(){
    return 'E';
}

Object* FactoryObjectCellExit::createObject(){
    return new ObjectCellExit;
}

//---------------------------------------------------------

//ObjectTrap
char const ObjectTrap::getName(){
    return '*';
}

Object* FactoryObjectTrap::createObject(){
    return new ObjectTrap;
}

//---------------------------------

//ObjectApple
char const ObjectApple::getName(){
    return 'o';
}

Object* FactoryObjectApple::createObject(){
    return new ObjectApple;
}
//---------------------------------

//ObjectWeapon
char const ObjectWeapon::getName(){
    return 'W';
}

Object* FactoryObjectWeapon::createObject(){
    return new ObjectWeapon;
}
//---------------------------------

//ObjectTool
char const ObjectTool::getName(){
    return '|';
}

Object* FactoryObjectTool::createObject(){
    return new ObjectTool;
}
//---------------------------------

