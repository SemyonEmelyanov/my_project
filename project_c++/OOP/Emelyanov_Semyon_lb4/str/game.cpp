#include "game.h"
void Game::StartGame(){
     std::string file_name = "game.txt";
    std::ifstream file(file_name);
    if (!(file)){
        std::cout << "You haven't entered correct input file." << std::endl;
    }
    else{
        std::string line;
        int counter = 0;
        int len;
        while (getline(file,line)){
            len = line.length();
            counter++;
        }
        file.clear();
        file.seekg(0);
        area = Field::getInstance();
        area->setSizeField(len,counter);
        char c;
        FieldIterator iter;
        std::cout<<iter.isDone();
        while(!(iter.isDone())){
            c = file.get();
            if (iter.endW()){
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextH();
            }
            else{
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextW();
            }            
        }
        file.close();
        //Player TOR;
        player = new Player;
        player->Startpos(area);
        player->nonblock(NB_ENABLE);
        //Context tr;
        context = new Context;
        cell_observer = new CellObserver();
        abstraction = new Abstraction(cell_observer);
        player->player_observer = new PlayerObserver();
        player->abstraction_player = new Abstraction( player->player_observer);
        player->Subscribe(player->abstraction_player);
        // while(player->status_exit!='E'){
        //         system("clear");
        //         this->GameShow(area,player);
        //         player->CoutPlayerInformation();
        //         this->GameLogic(area,player,context);
        //         context->DoSomeBusinessLogic(player, area,cell_observer,abstraction);
        //         player->Update(area);
        //         Sleep(INTERVAL);
        // }
    }
}

void Game::GamePlay(){
    system("clear");
    this->GameShow(area,player);
    player->CoutPlayerInformation();
    this->GameLogic(area,player,context);
    context->DoSomeBusinessLogic(player, area,cell_observer,abstraction);
}

void Game::MovePlayer(char symbol){
    // player->Update(area, symbol);
    switch (symbol) {
        case 'w': 
            if(area->CellInformationOpen(player->GetX_pos(),player->GetY_pos()-1)) player->SetY_pos(player->GetY_pos()-1);
            break;
        case 'a': 
            if(area->CellInformationOpen(player->GetX_pos()-1,player->GetY_pos())) player->SetX_pos(player->GetX_pos()-1);
            break;
        case 's': 
            if(area->CellInformationOpen(player->GetX_pos(),player->GetY_pos()+1)) player->SetY_pos(player->GetY_pos()+1);
            break;
        case 'd': 
            if(area->CellInformationOpen(player->GetX_pos()+1,player->GetY_pos())) player->SetX_pos(player->GetX_pos()+1);
            break;
    }
    player->Notify();
    player->status = "";
    Sleep(INTERVAL);
}
// void Game::GameOver(){
//     system("cls");
//     system("color 4F");
//     std::cout<<std::endl<<"Game over!"<<std::endl<<"press any key to exit!"<<std::endl;
//     exit(0);
// }

void Game::GameShow(Field* area, Player* player){
    for( int i = 0; i < area->getHight(); i++){
        for( int j = 0; j < area->getWidth(); j++){
            if(player->Playerpos(j,i)){
                std::cout<<"\x1b[47;35;1m@\x1b[0m";
            }
            else{
                std::cout<<area->CellField(j,i);
            }
        }
        std::cout<<"\n";
    }
}

void Game::GameLogic(Field* area, Player* player, Context* strategy){
    if(player->GetApple()>6 && player->GetTrap()>2 && player->GetTool()>1){
        strategy->SetStrategy(new Strategy2);
    }
    else{
        strategy->SetStrategy(new Strategy1);
    }
}


Game::~Game(){
    
}

void Game::EndGame(){
    delete cell_observer;
	delete abstraction;
    delete area;
    delete player;
    delete context;
    std::cout<<"Спасибо за игру!\n";
    Sleep(1000);
}

 Player* Game::GetPlayer(){
     return this->player;
 }