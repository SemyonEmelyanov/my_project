#include "menu.h"

Menu::~Menu(){
    delete csg;
    delete ceg;
    delete cmp;
}

void Menu::menu(){
    char flag;
    Game* game;
    std::cout<<"Приветствуем Вас в игре бродилке!\n Для новой игры нажмите S\n Для выхода нажмите E\n ";
    while(true){
        std::cin >> flag;
        if(flag == 'S'){
            game = new Game;
            csg->execute(game);
            while(cmp->symbol!= 'E' && game->GetPlayer()->status_exit!='E'){
                game->GamePlay();
                if(Kbhit()!=0){
                    cmp->symbol = fgetc(stdin);
                    cmp->execute(game);
                }
                Sleep(200);
            }
            cmp->symbol = 'S';
        }
        if(flag == 'E'){
            ceg->execute(game);
            //system("clear");
            break;
        }
        system("clear");
        std::cout<<"Хотите новую игру или выйти?\n";
        delete game;

    }
    system("clear");
    // delete game;
}

void Menu::nonblock(int state) {
  struct termios ttystate;

  // Get the terminal state.
  tcgetattr(STDIN_FILENO, &ttystate);

  if (state == NB_ENABLE) {
    // Turn off canonical mode.
    ttystate.c_lflag &= ~ICANON;
    // Minimum of number input read.
    ttystate.c_cc[VMIN] = 1;
  } else if (state == NB_DISABLE) {
    // Turn on canonical mode.
    ttystate.c_lflag |= ICANON;
  }
  // Set the terminal attributes.
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

int Menu::Kbhit(){
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}