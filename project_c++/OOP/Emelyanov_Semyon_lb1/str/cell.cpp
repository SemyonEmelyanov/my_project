#include "cell.h"


Cell::Cell(){
    object = EMPTY;
}

Cell::~Cell(){}

std::string Cell::isOpen(){
    bool isOpen = false;
    if (object == EMPTY||object == EXIT || object == START){
        isOpen = true;
    }
    std::string out = (isOpen ? "open" : "close");
    return "This is " + out + " cell";
}

std::string Cell::Information(){
    if(object == EMPTY)
        return " ";
    if(object == BUSY)
        return "#";
    if(object == EXIT)
        return "E";
    if(object == START)
        return "S";
}
