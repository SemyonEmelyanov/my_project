#include "cell.h"

class FieldIterator{
	public:
	FieldIterator();
	void nextW();
	bool isDone();
	void nextH();
	bool endW();	
	int indexW;
	int indexH;
};

class Field{
private:
	Cell** cells;
	Field();
	~Field();
	static Field *instance;
	Field(Field const& other) = delete;
	Field& operator = (Field const& other) = delete;
	Field(Field && other) = delete;
	Field& operator = (Field && other) = delete;
	int width;
	int hight;
public:
	friend class FieldIterator;
	static Field* getInstance();
	void setSizeField(int width, int hight);
	int getWidth();
	int getHight();
	void ReadField();
	void setCell(char c,int x, int y);
	void ShowField();
	void CellInformation(int x, int y);
};

