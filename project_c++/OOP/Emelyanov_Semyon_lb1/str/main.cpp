#include "field.h"
#include <fstream>
#define Sleep(x) usleep(x*2000)
#define INTERVAL 500
int main(){
    std::string file_name = "game.txt";
    std::ifstream file(file_name);
    if (!(file)){
        std::cout << "You haven't entered correct input file." << std::endl;
    }
    else{
        std::string line;
        int counter = 0;
        int len;
        while (getline(file,line)){
            len = line.length();
            counter++;
        }
        file.clear();
        file.seekg(0);
        Field* area = Field::getInstance();
        area->setSizeField(len,counter);
        char c;
        FieldIterator iter;
        /*
        int x = 0;
        int y = 0;
        while(!file.eof()){
            c = file.get();
            if (x == len){
                area->setCell(c,x,y);
                x = 0;
                y++;
            }
            else{
                area->setCell(c,x,y);
                x++;
            }            
        }
        */
        std::cout<<iter.isDone();
        while(!(iter.isDone())){
            c = file.get();
            if (iter.endW()){
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextH();
            }
            else{
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextW();
            }            
        }
        
    while(true){
        std::cout<<"Для получения информации о клетке введите I !\n";
        std::cout<<"Для завершения программы нажмите Q !\n";
        area->ShowField();
        char flag;
        std::cin>>flag;
        switch(flag){
            case 'Q':
            system("clear");
            return 0;
            case 'I':
            std::cout<<"Введите две координаты через пробел!_";
            int x,y;
            std::cin>>x>>y; 
            area->CellInformation(x,y);
        }


        Sleep(INTERVAL);
        system("clear");
    }

    }                
    return 0;
}