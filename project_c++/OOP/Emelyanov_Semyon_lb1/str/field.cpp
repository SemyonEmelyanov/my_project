#include "field.h"
Field* Field::instance = nullptr;
Field::Field()
{
    width = 10;
	hight = 10;
    cells = new Cell * [hight];
    for (int i = 0; i < hight; i++)
        cells[i] = new Cell[width];   
}

Field::~Field() {
    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;
}


/*
Field::Field(Field const& other):
    width(other.width),
    hight(other.hight)
{
    cells = new Cell* [hight];
    for(int i = 0; i < hight; i++){
        cells[i] = new Cell [width];
        for(int j = 0; j < width; j++)
            cells[i][j] = other.cells[i][j];
    }
}

Field& Field::operator=(Field const& other){
    if (this == &other)
        return *this;

    for(int i = 0; i < hight; i++)
        delete [] cells[i];
    delete [] cells;


    width = other.width;
    hight = other.hight;
    cells = new Cell* [hight];
    for(int i = 0; i < hight; i++){
        cells[i] = new Cell [width];
        for(int j = 0; j < width; j++)
            cells[i][j] = other.cells[i][j];
    }
    return *this;
}
*/
/*
Field::Field(Field&& other) {
    cells = other.cells;
    other.cells = nullptr;
}

Field& Field::operator=(Field&& other) {
    if (this == &other)
        return *this;

    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;

    cells = other.cells;
    other.cells = nullptr;

    return *this;
}
*/



Field* Field::getInstance(){
    if (instance == nullptr ){
        instance = new Field();
    }
    return instance; 
}

void Field::setSizeField(int w, int h){
    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;
    width = w;
    hight = h;
    cells = new Cell * [hight];
    for (int i = 0; i < hight; i++)
       cells[i] = new Cell[width];       
}

int Field::getHight(){
    return hight;
}

int Field::getWidth(){
    return width;
}

void Field::setCell(char c,int x, int y){
    switch(c){
        case '#':
        cells[y][x].object = BUSY;
        break;
        case ' ':
        cells[y][x].object = EMPTY;
        break;
        case 'S':
        cells[y][x].object = START;
        break;
        case 'E':
        cells[y][x].object = EXIT;
        break;
    }

}

void Field::ShowField(){
    for( int i = 0; i < this->getHight(); i++){
        for( int j = 0; j < this->getWidth(); j++){
            std::cout<<cells[i][j].Information();
        }
        std::cout<<"\n";
    }
}
void Field::CellInformation(int x,int y){
    std::cout<<cells[y][x].isOpen()<<std::endl;
}



FieldIterator::FieldIterator(){
    indexH = 0;
    indexW = 0;
}

void FieldIterator::nextW() {
        indexW++;
}
void FieldIterator::nextH(){
    if (indexW == Field::instance->getWidth()) {
        indexH++;
        indexW = 0;
    }
}

bool FieldIterator::endW() {
    bool flag = false;
    if (indexW == Field::instance->getWidth()){
        flag = true;
    }
    return flag;
}

bool FieldIterator::isDone() {
    return (indexH == Field::instance->getHight());
}

