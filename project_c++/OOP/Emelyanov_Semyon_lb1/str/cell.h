#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <termios.h>
#include <sys/select.h>
#include <memory.h>
enum CellObject{EMPTY,EXIT,START,BUSY};

class Cell{
public:
	Cell();
	~Cell();
	CellObject object; 
	std::string isOpen();
	std::string Information();
};