#include "game.h"
void Game::StartGame(){
     std::string file_name = "game.txt";
    std::ifstream file(file_name);
    if (!(file)){
        std::cout << "You haven't entered correct input file." << std::endl;
    }
    else{
        std::string line;
        int counter = 0;
        int len;
        while (getline(file,line)){
            len = line.length();
            counter++;
        }
        file.clear();
        file.seekg(0);
        Field* area = Field::getInstance();
        area->setSizeField(len,counter);
        char c;
        FieldIterator iter;
        std::cout<<iter.isDone();
        while(!(iter.isDone())){
            c = file.get();
            if (iter.endW()){
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextH();
            }
            else{
                area->setCell(c,iter.indexW,iter.indexH);
                iter.nextW();
            }            
        }
        file.close();
        Player TOR;
        TOR.Startpos(area);
        TOR.nonblock(NB_ENABLE);
        Context tr;
        cell_observer = new CellObserver();
        abstraction = new Abstraction(cell_observer);
        TOR.player_observer = new PlayerObserver();
        TOR.abstraction_player = new Abstraction( TOR.player_observer);
        TOR.Subscribe(TOR.abstraction_player);
        while(true){
            system("clear");
            this->GameLogic(area,&TOR,&tr);
            tr.DoSomeBusinessLogic(&TOR, area,cell_observer,abstraction);
            this->GameShow(area,&TOR);
            TOR.CoutPlayerInformation();
            TOR.Update(area);
            Sleep(INTERVAL);
            //system("clear");
        }
    }
}

void Game::GameOver(){
    system("cls");
    system("color 4F");
    std::cout<<std::endl<<"Game over!"<<std::endl<<"press any key to exit!"<<std::endl;
    exit(0);
}

void Game::GameShow(Field* area, Player* player){
    for( int i = 0; i < area->getHight(); i++){
        for( int j = 0; j < area->getWidth(); j++){
            if(player->Playerpos(j,i)){
                std::cout<<"\x1b[47;35;1m@\x1b[0m";
            }
            else{
                std::cout<<area->CellField(j,i);
            }
        }
        std::cout<<"\n";
    }
}

void Game::GameLogic(Field* area, Player* player, Context* strategy){
    if(player->GetApple()>6 && player->GetTrap()>2 && player->GetTool()>1){
        strategy->SetStrategy(new Strategy2);
    }
    else{
        strategy->SetStrategy(new Strategy1);
    }
}