#pragma once 
#include "cell.h"

class FieldIterator{
	public:
	FieldIterator();
	void nextW();
	bool isDone();
	void nextH();
	bool endW();	
	int indexW;
	int indexH;
};

class Field{
private:
	Cell** cells;
	Field();
	~Field();
	static Field *instance;
	Field(Field const& other);
	Field& operator = (Field const& other);
	Field(Field && other);
	Field& operator = (Field && other);
	int width;
	int hight;
	int x_start_pos;
	int y_start_pos; 	
public:
	friend class FieldIterator;
	static Field* getInstance();
	void setSizeField(int width, int hight);
	int getWidth();
	int getHight();
	int getXpos();
	int getYpos();	
	void ReadField();
	void Operation(int x, int y, Abstraction*);
	void setCell(char c,int x, int y);
	// void ShowField();
	bool CellInformationOpen(int x, int y);
	// void PlayerCell(int x, int y);
	// void EmptyCell(int x, int y);
	std::string CellField(int x, int y);
	Object* CellObject(int x, int y);
	void NewCellObject(int x, int y, Object* object);
};

