#pragma once 
#include "object.h" 
#include "AbstractObserver.h"
#include "publisher.h"
#define FILE_NAME "logs_cell.txt"

class Cell:public Publisher{
protected: 
	std::list <Abstraction*> list_of_observers;

public:
	int x_coord;
	int y_coord;
	Cell();
	~Cell();
	Object* object; 
	bool isOpen();
	std::string Information();
	void Subscribe(Abstraction* abstraction) override;
    void Unsubscribe(Abstraction* abstraction) override;
    void Notify() override;
	friend std::ostream& operator<<(std::ostream &out, Cell* cell);

};

class CellObserver:public AbstractObserver{
   std::fstream file;
public:
    CellObserver();
    ~CellObserver();
    void update(Publisher* cell)override;
};

