#include "cell.h"


Cell::Cell(){
    object = new ObjectCellEmpty;
}

Cell::~Cell(){}

bool Cell::isOpen(){
    if (object->getName() == ' '||object->getName() == 'T'||object->getName() == 'o'||object->getName() == '|'||object->getName() == '*'||object->getName() == 'E'){
        return true;
    }
    else{
        return false;
    }
}

std::string Cell::Information(){
    switch(object->getName()){
        case ' ':
            return "\x1b[47m \x1b[0m";
        case '#':
            return "\x1b[47;30;1m#\x1b[0m";
        case 'o':
            return "\x1b[47;31mo\x1b[0m";
        case '|':
            return "\x1b[47;36m|\x1b[0m";
        case '*':
            return "\x1b[47;30m*\x1b[0m";
        case 'W':
            return "\x1b[47;36mW\x1b[0m";
        case 'E':
            return "\x1b[42;31;1mE\x1b[0m";
        case 'S':
            return "@";
        case '@':
            return "@";
    }
}

std::ostream& operator<<(std::ostream &out, Cell* cell){
        out << "Object taken from the cell: " << cell->object->getName() << " ["<< cell->x_coord<< ","<<cell->y_coord<< "] \n";
        return out;


}



void Cell::Subscribe(Abstraction* abstraction){
    this->list_of_observers.push_back(abstraction);
}

void Cell::Unsubscribe(Abstraction* abstraction){
    this->list_of_observers.remove(abstraction);
}

void Cell::Notify(){
    std::list<Abstraction*>::iterator iterator = list_of_observers.begin();
            while (iterator != list_of_observers.end()) {
              //std::cout << this;
              (*iterator)->Operation(this);
              ++iterator;
        }
}



CellObserver::CellObserver(){
    file.open(FILE_NAME, std::fstream::in | std::fstream::out | std::fstream::app);
}
CellObserver::~CellObserver(){
    file.close();
}
void CellObserver::update(Publisher* cell){
    std::cout << dynamic_cast<Cell*>(cell);
    file << dynamic_cast<Cell*>(cell);
}