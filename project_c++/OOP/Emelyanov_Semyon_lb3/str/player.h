#pragma once 
#include <iostream>
#include "field.h"
#define STDIN_FILENO 0
#define NB_DISABLE 0
#define NB_ENABLE 1
#define FILE_NAME_PLAYER "logs_player.txt"
class PlayerObserver;
class Player:public Publisher{
    private: 
        int x_pos;
        int y_pos;
        int apple;  
        int tool;
        int trap;
        bool st = false;    
        std::string status = "";   
        std::list<Abstraction*> list_of_observers;
    public:
        Player();
        void Update(Field* area);
        void Startpos(Field* area);
        int Kbhit();
        void nonblock(int state);
        bool Playerpos(int x, int y);
        void CoutPlayerInformation();
        int GetCoordx();
        int GetCoordy();
        void operator<<(Object* object);
        bool HappyPlayer();
        int GetApple();
        int GetTrap();
        int GetTool();
        PlayerObserver* player_observer;
        Abstraction* abstraction_player;
        //----------------------------
        void Subscribe(Abstraction* abstraction)override;
        void Unsubscribe(Abstraction* abstraction)override;
        void Notify()override;
        friend std::ostream &operator<<(std::ostream &out, Player* player);
};


class PlayerObserver:public AbstractObserver{
protected:
    std::fstream file;
public:
    PlayerObserver();
    ~PlayerObserver();
    void update(Publisher* player)override;
};

