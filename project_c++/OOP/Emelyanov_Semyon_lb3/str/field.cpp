#include "field.h"
Field* Field::instance = nullptr;
Field::Field()
{
    width = 10;
	hight = 10;
    cells = new Cell * [hight];
    for (int i = 0; i < hight; i++)
        cells[i] = new Cell[width];   
}

Field::~Field() {
    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;
}


Field::Field(Field const& other):
    width(other.width),
    hight(other.hight)
{
    cells = new Cell* [hight];
    for(int i = 0; i < hight; i++){
        cells[i] = new Cell [width];
        for(int j = 0; j < width; j++)
            cells[i][j] = other.cells[i][j];
    }
}

Field& Field::operator=(Field const& other){
    if (this == &other)
        return *this;

    for(int i = 0; i < hight; i++)
        delete [] cells[i];
    delete [] cells;


    width = other.width;
    hight = other.hight;
    cells = new Cell* [hight];
    for(int i = 0; i < hight; i++){
        cells[i] = new Cell [width];
        for(int j = 0; j < width; j++)
            cells[i][j] = other.cells[i][j];
    }
    return *this;
}

Field::Field(Field&& other) {
    cells = other.cells;
    other.cells = nullptr;
}

Field& Field::operator=(Field&& other) {
    if (this == &other)
        return *this;

    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;

    cells = other.cells;
    other.cells = nullptr;

    return *this;
}

Field* Field::getInstance(){
    if (instance == nullptr ){
        instance = new Field();
    }
    return instance; 
}

void Field::setSizeField(int w, int h){
    for (int i = 0; i < hight; i++)
        delete[] cells[i];
    delete[] cells;
    width = w;
    hight = h;
    cells = new Cell * [hight];
    for (int i = 0; i < hight; i++)
       cells[i] = new Cell[width];       
}

int Field::getHight(){
    return hight;
}

int Field::getWidth(){
    return width;
}

void Field::setCell(char c,int x, int y){
    FactoryObjectBusy busy;
    FactoryObjectCellExit exit;
    FactoryObjectCellEmpty empty;
    FactoryObjectTrap trap;
    FactoryObjectApple apple;
    FactoryObjectWeapon weapon;
    FactoryObjectTool tool;
    switch(c){
        case '#':
        cells[y][x].object = busy.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case ' ':
        cells[y][x].object = empty.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case 'S':
        x_start_pos = x;
        y_start_pos = y;
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case 'E':
        cells[y][x].object = exit.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case '@':
        x_start_pos = x;
        y_start_pos = y;
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case 'o':
        cells[y][x].object = apple.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case '*':
        cells[y][x].object = trap.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case '|':
        cells[y][x].object = tool.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;
        case 'W':
        cells[y][x].object = weapon.createObject();
        cells[y][x].x_coord = x;
        cells[y][x].y_coord = y;
        break;        
    }

}

int Field::getXpos(){
    return x_start_pos;
}
int Field::getYpos(){
    return y_start_pos;
}

// void Field::ShowField(){
//     for( int i = 0; i < this->getHight(); i++){
//         for( int j = 0; j < this->getWidth(); j++){
//             std::cout<<cells[i][j].Information();
//         }
//         std::cout<<"\n";
//     }
// }

bool Field::CellInformationOpen(int x,int y){
    return cells[y][x].isOpen();
}

std::string Field::CellField(int x, int y){
    return cells[y][x].Information();
}

Object* Field::CellObject(int x, int y){
    return cells[y][x].object;
}

void Field::NewCellObject(int x, int y, Object* object){
    delete cells[y][x].object;
    cells[y][x].object = object;
}
void Field::Operation(int x, int y, Abstraction* abstraction){
    cells[y][x].Subscribe(abstraction);
    cells[y][x].Notify();
    cells[y][x].Unsubscribe(abstraction);
}

//------------------------------------------------------------------------

FieldIterator::FieldIterator(){
    indexH = 0;
    indexW = 0;
}

void FieldIterator::nextW() {
        indexW++;
}
void FieldIterator::nextH(){
    if (indexW == Field::instance->getWidth()) {
        indexH++;
        indexW = 0;
    }
}

bool FieldIterator::endW() {
    bool flag = false;
    if (indexW == Field::instance->getWidth()){
        flag = true;
    }
    return flag;
}

bool FieldIterator::isDone() {
    return (indexH == Field::instance->getHight());
}

//---------------------------------------------------------------------