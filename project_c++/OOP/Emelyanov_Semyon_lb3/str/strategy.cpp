#include "strategy.h"
Context::~Context() {

	delete this->strategy;
}

void Context::SetStrategy(Strategy* strategy) {

    delete this->strategy;
    this->strategy = strategy;
}

void Context::DoSomeBusinessLogic(Player* player, Field* area,CellObserver* cell_observer , Abstraction* abstraction) {

    this->strategy->doStrategy(player,area,cell_observer ,abstraction);
}

void Strategy1::doStrategy(Player* player, Field* area,CellObserver* cell_observer , Abstraction* abstraction) {

    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='o'){
        *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
        area->Operation(player->GetCoordx(), player->GetCoordy(),abstraction);
        //player->Subscribe(player->abstraction_player);
        // player->Notify();
        //player->Unsubscribe(player->abstraction_player);
        area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
    }
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='|'){
        if (player->GetTool()+player->GetTrap()<player->GetApple()){
            *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
            area->Operation(player->GetCoordx(), player->GetCoordy(),abstraction);
            //player->Subscribe(player->abstraction_player);
            //player->Notify();
            //player->Unsubscribe(player->abstraction_player);
            area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
        }

    }
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='*'){
        if ((player->GetTool()!=0)&&(player->GetTool()+player->GetTrap()<player->GetApple())){
            *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
            area->Operation(player->GetCoordx(), player->GetCoordy(),abstraction);
            //player->Subscribe(player->abstraction_player);
            //player->Notify();
            //player->Unsubscribe(player->abstraction_player);
            area->NewCellObject(player->GetCoordx(), player->GetCoordy(), new ObjectCellEmpty);
        }
    }
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()==' '){
        *player << area->CellObject(player->GetCoordx(), player->GetCoordy());
        // player->Subscribe(player->abstraction_player);
        // player->Notify();
        //player->Unsubscribe(player->abstraction_player);
    }
    
}

void Strategy2::doStrategy(Player* player, Field* area,CellObserver* cell_observer , Abstraction* abstraction) {
    if(area->CellObject(player->GetCoordx(), player->GetCoordy())->getName()=='E'){
        std::cout<<'\n'<<"\x1b[43;31mПоздравляю, Вы выиграли!!!\x1b[0m"<<std::endl;
        delete cell_observer;
        delete player->player_observer;
        exit(1);
    }
    
}