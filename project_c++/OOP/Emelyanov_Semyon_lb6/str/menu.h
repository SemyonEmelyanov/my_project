#pragma once
#include "game.h"
#include "comand.h"

class Menu{
    CommandStartGame* csg = new CommandStartGame;
    CommandEndGame* ceg = new CommandEndGame;
    CommandMovePlayer* cmp = new CommandMovePlayer;
    SaveGame* sgame;
    LoadGame* lgame;
    public:
        ~Menu();
        void menu();
        int Kbhit();
        void nonblock(int state);
};
