#pragma once 
class AbstractObserver;
class Abstraction;

class Publisher{
    public:
        virtual void Subscribe(Abstraction* abstraction) = 0;
        virtual void Unsubscribe(Abstraction* abstraction) = 0;
        virtual void Notify() = 0;
};
