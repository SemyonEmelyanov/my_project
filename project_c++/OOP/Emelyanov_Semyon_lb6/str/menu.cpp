#include "menu.h"

Menu::~Menu(){
    delete csg;
    delete ceg;
    delete cmp;
}

void Menu::menu(){
    char flag;
    Game* game;
    std::string file_load = "game_load.txt";
    std::fstream file;
    std::string load_game;
    std::string save_game;
    std::cout<<"Приветствуем Вас в игре бродилке!\n";
    while(true){
        std::cout<<"Хотите новую игру (S) или выйти (E) или загрузить игру (L)?\n";
        std::cin >> flag;
        if(flag == 'S'){
            game = new Game;
            csg->execute(game);
            while(cmp->symbol!= 'E' && game->GetPlayer()->status_exit!='E'){
                game->GamePlay();
                if(Kbhit()!=0){
                    cmp->symbol = fgetc(stdin);
                    cmp->execute(game);
                }
                Sleep(200);
                if (cmp->symbol == 'T'){
                    file.open(file_load, std::fstream::in | std::fstream::out | std::fstream::app);
                    std::cout<<"Введите название файла: ";
                    std::cin >> save_game;
                    sgame = new SaveGame (&save_game, game);
                    delete sgame;
                    file << save_game <<"\n"<< std::endl;
                    file.close();
                    cmp->symbol = ' ';
                }
            }
            cmp->symbol = 'S';
            delete game;
        }
        else if(flag == 'L'){
            file.open(file_load, std::fstream::in | std::fstream::out | std::fstream::app);
            std::cout<<"Доступные сохранения: "<<std::endl;
            while (getline(file,load_game)){
                std::cout<<load_game<<std::endl;
            }
            std::cout<<"Введите название файла: ";
            std::cin >> load_game;
            std::cout<<"\n";
            game = new Game;
            bool FLAG = true;
            lgame = new LoadGame (load_game,game, FLAG);
            delete lgame;
            file.close();
            //exit(1);
            if (FLAG){
                while(cmp->symbol!= 'E' && game->GetPlayer()->status_exit!='E'){
                    game->GamePlay();
                    if(Kbhit()!=0){
                        cmp->symbol = fgetc(stdin);
                        cmp->execute(game);
                    }
                    Sleep(200);
                    if (cmp->symbol == 'T'){
                        file.open(file_load, std::fstream::in | std::fstream::out | std::fstream::app);
                        std::cout<<"Введите название файла: ";
                        std::cin >> save_game;
                        sgame = new SaveGame (&save_game, game);
                        delete sgame;
                        file << save_game <<"\n"<< std::endl;
                        file.close();
                        cmp->symbol = ' ';
                    }
                }
            }
            else {
                std::cout<< "Ошибка! Файл не исправен!";
            }
            cmp->symbol = 'S';
        } 
        else if(flag == 'E'){
            //ceg->execute(game);
            //system("clear");
            break;
        }
        system("clear");

    }
    system("clear");
    // delete game;
}

void Menu::nonblock(int state) {
  struct termios ttystate;

  // Get the terminal state.
  tcgetattr(STDIN_FILENO, &ttystate);

  if (state == NB_ENABLE) {
    // Turn off canonical mode.
    ttystate.c_lflag &= ~ICANON;
    // Minimum of number input read.
    ttystate.c_cc[VMIN] = 1;
  } else if (state == NB_DISABLE) {
    // Turn on canonical mode.
    ttystate.c_lflag |= ICANON;
  }
  // Set the terminal attributes.
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

int Menu::Kbhit(){
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}