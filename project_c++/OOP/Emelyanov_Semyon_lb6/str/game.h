#pragma once 
#include "player.h"
#include <fstream>
#include "strategy.h"
#include "enemy.h"
#include "state.h"
#define Sleep(x) usleep(x*2000)
#define INTERVAL 500

class Game {
    CellObserver* cell_observer;
	Abstraction* abstraction;
    Field* area;
    Player* player;
    Context* context;
    friend class LoadGame;
    int CountEnemy = 3;
    int CountStatus = 0;
    Enemy<type>** enemy = new Enemy<type>*[CountEnemy];
    State* state ;//= new StadingState(enemy, CountStatus);
    void GameShow(Field* area, Player* player);   
    void GameLogic(Field* area, Player* player,Context*);
    std::string StatusState(){
        if (state == nullptr ){
            return "Error";
        }
        if(state->getTypeState() == 0){
            return "Сейчас враги спят!\n";
        }
        if(state->getTypeState() == 1){
            return "Сейчас враги активны!\n";
        }
        if(state->getTypeState() == 2){
            return "Сейчас враги в яростном режиме!\n";
        }
    }
    public:
        Enemy<type>** Getenemy(){
            return enemy;
        }
        ~Game();
        void StartGame();
        void MovePlayer(char symbol);
        //void GameOver();
        void EndGame();
        void GamePlay();
        Player* GetPlayer();
        int GetCountStatus(){
            return CountStatus;
        }
        int GetCountEnemy(){
            return CountEnemy;
        }
        void SetStatusEnemy(){
            if(CountStatus == 0){
                delete this->state;
                this->state = new StadingState(enemy, CountEnemy);
            }
            if(CountStatus == 10){
                delete this->state;
                this->state = new RunState(enemy, CountEnemy);
            }
            if(CountStatus == 30){
                delete this->state;
                this->state = new RageState(enemy, CountEnemy);
                CountStatus = -10;
            }
        }
        void EnemyMove(){
            for(int i = 0; i < CountEnemy; i++){
                enemy[i]->update();
                *enemy[i]<<player;
            }
            CountStatus++;
        }
};

class SaveGame{
private: 
    std::ofstream file;
    Field* instance;
public:
    SaveGame(std::string* save_game, Game* game){
        std::time_t now = std::time(0);
        tm* timeinfo = localtime(&now);
        char s[50];
        strftime(s, 50, "%F_%T.txt",timeinfo);
        *save_game += s;
        *save_game += "\n";
        file.open(*save_game);
        file << "player\n"<< game->GetPlayer()->GetX_pos()<< ":"<< game->GetPlayer()->GetY_pos()<<":"<<game->GetPlayer()->GetApple() << ":" << game->GetPlayer()->GetTool() << ":" << game->GetPlayer()->GetWeapon() << ":" << game->GetPlayer()->GetTrap() << std::endl;
        instance = Field::getInstance();
        file << "CountStatus\n"<< game->GetCountStatus()<<std::endl;
        file << "CountEnemy\n"<< game->GetCountEnemy()<<std::endl;
        for (int i = 0; i < game->GetCountEnemy();i++){
            file << game->Getenemy()[i]->GetNameSave()<<"\n"<< game->Getenemy()[i]->Getx() <<"\n"<< game->Getenemy()[i]->Gety() <<"\n"<< game->Getenemy()[i]->Getdx() <<"\n"<< game->Getenemy()[i]->Getdy()<<std::endl;
        }
        file << "Field\n" << instance->getHight()<<"\n"<<instance->getWidth()<<std::endl;
        for (int i = 0; i < instance->getHight();i++ ){
            for (int j = 0; j < instance->getWidth();j++){
                file<<instance->CellFieldSave(j,i);
            }
            file << std::endl;
        }
    }
    ~SaveGame(){
        file.close();
    }
};
class LoadGame{
private: 
    std::ifstream file;
public:
    LoadGame(std::string& loadfile, Game* game, bool& FLAG){
        std::string parametr;
        file.open(loadfile);
        std::string str;
        bool flag = true;
        //std::cout << parametr;
        while (getline(file,parametr) && flag){
            //std::cout << parametr;
            //exit(1);
            if(parametr == "player"){
                // std::cout << "fasf";
                // exit(1);
                getline(file, str);
                char symbol;
                int counter = 0;
                int result = 0;
                int i = 0;
                game->player = new Player;
               while(i < str.length() && flag){
                    switch(counter){
                        case 0:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                    break;
                                }
                                game->player->SetX_pos(result);
                                i++;
                                result = 0;
                            }
                            break;
                        case 1:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                }
                                game->player->SetY_pos(result);
                                i++;
                                result = 0;
                            }
                            break;    
                        case 2:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                }
                                game->player->SetApple(result);
                                i++;
                                result = 0;
                            }
                            break;
                        case 3:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                }
                                game->player->SetTool(result);
                                i++;
                                result = 0;
                            }
                            break;
                        case 4:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                }
                                game->player->SetWeapon(result);
                                i++;
                                result = 0;
                            }
                            break;
                        case 5:
                            symbol  = str[i];
                            result = result*10 + atoi(&symbol);
                            i++;
                            if (str[i] == ':'){
                                counter++;
                                if (!isdigit(str[i+1])){
                                    std::cout<<"\n"<<"Error!"<<std::endl;
                                    flag = false;
                                }
                                game->player->SetTrap(result);
                                i++;
                                result = 0;
                            }
                            break;
                        default:
                            std::cout<<"\n"<<"Error!"<<std::endl;
                            flag = false;


                    }

                }
            }
            if (parametr == "CountStatus"){
                getline(file, str);
                int result = 0;
                // for(int i = 0; i < str.length(), flag; i++){
                //     if(!isdigit(str[i]) && str[i] == '-'){
                //         std::cout<<"\n"<<"Error!"<<std::endl;
                //         flag = false;
                //     }
                // }
                if (flag) game->CountStatus = stoi(str);
                //exit(1);
            }
            if (parametr == "CountEnemy"){
                //exit(1);
                getline(file, str);
                int result = 0;
                if (flag) game->CountEnemy = stoi(str);
                //std::cout<< game->CountEnemy;
                std::string enemy;
                int countenemy = 0;
                //exit(1);
                for(int i = 0; i < game->CountEnemy && flag; i++){
                    getline(file, enemy);
                    if(enemy[0]=='&'){
                        getline(file, enemy);
                        int x = stoi(enemy);
                        getline(file, enemy);
                        int y = stoi(enemy);
                        getline(file, enemy);
                        int dx = stoi(enemy);
                        getline(file, enemy);
                        int dy = stoi(enemy);
                        game->enemy[i] = new EnemyEasy (x, y);
                        game->enemy[i]->Setdxdy(dx,dy);
                    }
                    if(enemy[0]=='$'){
                        getline(file, enemy);
                        int x = stoi(enemy);
                        getline(file, enemy);
                        int y = stoi(enemy);
                        getline(file, enemy);
                        int dx = stoi(enemy);
                        getline(file, enemy);
                        int dy = stoi(enemy);
                        game->enemy[i] = new EnemyHard (x, y);
                        game->enemy[i]->Setdxdy(dx,dy);
                    }
                }
                //exit(1);
            }
            if (parametr == "Field"){
                getline(file, str);
                int H = stoi(str);
                getline(file, str);
                int W = stoi(str);
                game->area = Field::getInstance();
                game->area->setSizeField(W,H);
                for (int i = 0; i < H; i++){
                    getline(file, str);
                    for (int j = 0; j < W; j++){
                        game->area->setCell(str[j],j,i);
                    }
                }

            }

        }
        if (flag){
            game->context = new Context;
            game->cell_observer = new CellObserver();
            game->abstraction = new Abstraction(game->cell_observer);
            game->player->player_observer = new PlayerObserver();
            game->player->abstraction_player = new Abstraction( game->player->player_observer);
            game->player->Subscribe(game->player->abstraction_player);
            //exit(1);
        }
        FLAG = flag;
    }
    ~LoadGame(){
        file.close();
    }
};