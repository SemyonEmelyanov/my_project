#pragma once
#include "enemy.h"
class State
{
public:

    virtual int getTypeState() = 0;
    virtual ~State() {}
};

class StadingState:public State
{
public:
    StadingState(Enemy<type>** enemy, int count){
        for(int i=0;i<count; i++){
            enemy[i]->status = "normal";
            enemy[i]->SaveMove();
            enemy[i]->Stading();
        }
    };

    int getTypeState() override{
        return 0;
    };

    ~StadingState(){};
};



class RunState:public State
{
public:
    RunState(Enemy<type>** enemy, int count){
        for(int i=0;i<count; i++){
            enemy[i]->Move();
        }
    };
    int getTypeState() override{
        return 1;
    };
    ~RunState(){};
};




class RageState:public State
{
public:
    RageState(Enemy<type>** enemy, int count){
        for(int i=0;i<count; i++){
            enemy[i]->status = "rage";
        }
    };
    int getTypeState() override{
        return 2;
    };
    ~RageState(){};
};