#pragma once
#include "player.h"
enum type {EASY, HARD};
template<typename T>
class Enemy
{
protected:
    std::string name;
    int x;
    int y;
    int dx;
    int dy;
    int savedx;
    int savedy;
    int attack;
    char namesave;
    Field* instance;
public:
    std::string status = "normal";
    Enemy(){
        instance = Field::getInstance();
    }
    std::string getName(){
        return name;
    };
    char GetNameSave(){
        return namesave;
    }
    void GetXY(){
        std::cout << "\n"<<x<<"  "<< y<<"\n";
    }
    int Getx(){
        return x;
    }
    int Gety(){
        return y;
    }
    int Getdx(){
        return dx;
    }
    int Getdy(){
        return dy;
    }
    void Setdxdy (int dx, int dy){
        this->dx = dx;
        this->dy = dy;
    }
    Enemy<T>* operator<<(Player* player){
        if (status == "rage"&& player->GetX_pos() == x && player->GetY_pos() == y){
            //exit(1);
            player->SetApple(-1);
        }
        else{
            if (player->GetWeapon()<attack && player->GetX_pos() == this->x && player->GetY_pos() == this->y){
                //exit(1);
                player->SetApple(player->GetWeapon()-attack);
            }
        }
    };
    void SaveMove(){
        this->savedx = this->dx;
        this->savedy = this->dy;
    }
    bool TestEnemy(int x, int y){
        if(this->x == x && this->y == y) return true;
        else return false;
    }
    void Stading(){
        this->dx = 0;
        this->dy = 0;
    }
    virtual void update() = 0;
    virtual void Move() = 0;
};

class EnemyEasy: public Enemy <type> {
    public:
        EnemyEasy(int x, int y){
            this->x = x;
            this->y = y;
            this->dx = 1;
            this->dy = 0;
            this->attack = 1;
            name = "\x1b[47;35;1m&\x1b[0m";
            namesave = '&';
        }
        void update()override{
            //exit(1);
            if(instance->CellInformationOpen(x+dx,y)) x= x + dx;
            else dx = dx * (-1);
        }
        void Move()override{
            //exit(1);
            this->dx = this->savedx;
            this->dy = this->savedy;
        }
};

class EnemyHard: public Enemy <type>{
    public:
        EnemyHard(int x, int y){
            this->x = x;
            this->y = y;
            this->dx = 0;
            this->dy = 1;
            this->attack = 2;
            name = "\x1b[47;35;1m$\x1b[0m";
            namesave = '$';
        }
        void update()override{
            if(instance->CellInformationOpen(x,y+dy)) y= y + dy;
            else dy = dy * (-1);
        }
        void Move()override{
            this->dx = this->savedx;
            this->dy = this->savedy;
        }

};