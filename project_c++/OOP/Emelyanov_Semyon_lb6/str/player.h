#pragma once 
#include <iostream>
#include "field.h"
#define STDIN_FILENO 0
#define NB_DISABLE 0
#define NB_ENABLE 1
#define FILE_NAME_PLAYER "logs_player.txt"
class PlayerObserver;
class Player:public Publisher{
    private: 
        int x_pos;
        int y_pos;
        int apple;  
        int tool;
        int trap;
        int weapon;
        bool st = false;     
        std::list<Abstraction*> list_of_observers;
    public:
        std::string save_status = "";
        std::string status = "";  
        char status_exit = 's';
        Player();
        ~Player();
        void Update(Field* area,char symbol);
        void Startpos(Field* area);
        int Kbhit();
        void nonblock(int state);
        bool Playerpos(int x, int y);
        void CoutPlayerInformation();
        int GetCoordx();
        void SetX_pos(int x);
        void SetY_pos(int y);
        int GetX_pos();
        int GetY_pos();
        int GetCoordy();
        void operator<<(Object* object);
        bool HappyPlayer();
        int GetTrap();
        int GetTool();
        int GetApple();
        int GetWeapon(){
            return this->weapon;
        }
        int SetApple(int f){
            this->apple = this->apple + f;
        }
        int SetTool(int f){
            this->tool = this->tool + f;
        }
        int SetWeapon(int f){
            this->weapon = this->weapon + f;
        }
        int SetTrap(int f){
            this->trap = this->trap + f;
        }
        PlayerObserver* player_observer;
        Abstraction* abstraction_player;
        //----------------------------
        void Subscribe(Abstraction* abstraction)override;
        void Unsubscribe(Abstraction* abstraction)override;
        void Notify()override;
        friend std::ostream &operator<<(std::ostream &out, Player* player);
};


class PlayerObserver:public AbstractObserver{
protected:
    std::fstream file;
public:
    PlayerObserver();
    ~PlayerObserver();
    void update(Publisher* player)override;
};

