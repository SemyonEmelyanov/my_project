#include <iostream>
#include <vector>
#include<time.h>
#include <map>
#include <set>
#include <algorithm>
using namespace std;
char start; 
char finish;
map<char, map<char, int>> graph;
map<char, map<char, int>> outGraph;
int maxvrtx;
set <char> path;

void readGraph(){
    int N, weight;
    char x,y, whitespace;
    cin>>N;
    cin>>start>>finish;
    maxvrtx = 0;
    for (int i = 0; i < N; i++){
        cin>>x>>y>>weight;
        if(maxvrtx < weight) maxvrtx = weight;
        graph[x][y] = weight;
        outGraph[x][y] = 0;
    }
    maxvrtx++;
    for (auto& i : graph)
    {
        for (auto& j : i.second)
        {
            outGraph[i.first][j.first] = 0;
        }
    }
} 

int dfs(char current_key, int Cmin ){
    vector<char> keylist;

    for ( auto& elem: graph[current_key])
    {
        keylist.push_back(elem.first);
    }
    reverse(keylist.begin(), keylist.end());
    if( current_key == finish ) return Cmin;
    path.insert(current_key);
    for (char& next_key: keylist)
    {
        if( !path.count(next_key) && graph[current_key][next_key] > 0) 
        {
            //cout<<min(Cmin, graph[current_key][next_key])<<" ";
            int delta = dfs(next_key, min(Cmin, graph[current_key][next_key]));
            if( delta > 0 ) {
                //cout<<delta<<":"<<graph[current_key][next_key]<<" ";
                graph[current_key][next_key]-= delta;
                outGraph[current_key][next_key]+= delta;
                return delta;
            }
        }
    }
    return 0;
}

int main(){
    bool flag = true;
    int potok = 0;
    int a = 0;
    readGraph();
    while (1){
        path.clear();
        path.insert(start);
        a = dfs(start, maxvrtx);
        flag = a > 0 && a != maxvrtx;
        if(flag){
            potok += a;
        }
        else{
            break;
        } 
    }
    cout << potok << "\n";
    for (auto& i : outGraph)
    {
        for (auto& j : i.second)
        {
            cout << i.first << " "<< j.first << " " << j.second << "\n";
        }   
    }
    return 0;    
}