#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
using namespace std;

int getAdvanceVertex(int v, char ch, bool suff = false);
const int numberAlphabetCharacters = 27;
char J = -1;

class Vertex{
    public: 
        int children[numberAlphabetCharacters], ix, suffixIx, advanceVertexes[numberAlphabetCharacters], par, goodSuffixIx;
        bool flag;
        char symbol;
        Vertex(int par, char symbol){
            for (int i = 0; i < numberAlphabetCharacters; i++) {
                this->children[i] = this->advanceVertexes[i] = -1;
            }
            this->flag = false;
            this->suffixIx = -1;
            this->par = par;
            this->symbol = symbol;
            this->goodSuffixIx = -1;
        }
};
vector<Vertex> bohr;
vector<string> patterns;

int functionIndex(char symbol){
    return (symbol)==J?(numberAlphabetCharacters-1):( (symbol)>='a'&&(symbol)<='z'?(symbol)-'a':((symbol)>='A'&&(symbol)<='Z'?(symbol)-'A':(symbol)) );
}

void addString(const string &s) {
    bohr.push_back(Vertex(0, '\0'));
    int curLast = 0;
    for (int i = 0; i < s.length(); i++) {
        char ch = functionIndex(s[i]);
        if (bohr[curLast].children[ch] == -1) {
            bohr.push_back(Vertex(curLast, ch));
            bohr[curLast].children[ch] = bohr.size() - 1;
        }
        curLast = bohr[curLast].children[ch];
    }
    bohr[curLast].flag = true;
    patterns.push_back(s);
    bohr[curLast].ix = patterns.size() - 1;
}

int getSuffixIx(int v) {
    if (bohr[v].suffixIx == -1)
        if (v == 0 || bohr[v].par == 0)
            bohr[v].suffixIx = 0;
        else
            bohr[v].suffixIx = getAdvanceVertex(getSuffixIx(bohr[v].par), bohr[v].symbol, true);
    return bohr[v].suffixIx;
}

int getAdvanceVertex(int v, char ch, bool suff) {
    suff = ch == functionIndex(J);
    if (bohr[v].advanceVertexes[ch] == -1)
        if (bohr[v].advanceVertexes[functionIndex(J)] == -1 || suff) {
            if (bohr[v].children[ch] != -1) {
                bohr[v].advanceVertexes[ch] = bohr[v].children[ch];
            } else if (bohr[v].children[functionIndex(J)] != -1 && !suff) {
                bohr[v].advanceVertexes[ch] = bohr[v].children[functionIndex(J)];
            } else if(ch == functionIndex(J)){
                int cl = -1;
                for(int i = 0; i<numberAlphabetCharacters;i++){
                    if(bohr[v].children[i]!=-1){
                        cl = bohr[v].children[i];
                    }
                }
                if(cl!=-1){
                    bohr[v].advanceVertexes[ch] = cl;
                }else if (v == 0)
                    bohr[v].advanceVertexes[ch] = 0;
                else {
                    auto sf = getSuffixIx(v);
                    bohr[v].advanceVertexes[ch] = getAdvanceVertex(sf, ch);
                }
            } else if (v == 0)
                bohr[v].advanceVertexes[ch] = 0;
            else {
                auto sf = getSuffixIx(v);
                bohr[v].advanceVertexes[ch] = getAdvanceVertex(sf, ch);
            }
        } else {
            bohr[v].advanceVertexes[ch] = bohr[v].advanceVertexes[functionIndex(J)];
        }
    return bohr[v].advanceVertexes[ch];

}

int getGoodSuffixIx(int v) {
    if (bohr[v].goodSuffixIx == -1) {
        int u = getSuffixIx(v);
        if (u == 0)
            bohr[v].goodSuffixIx = 0;
        else
            bohr[v].goodSuffixIx = (bohr[u].flag) ? u : getGoodSuffixIx(u);
    }
    return bohr[v].goodSuffixIx;
}

void follow(int v, int i, vector<pair<int, int>> &occ) {
    for (int u = v; u != 0; u = getGoodSuffixIx(u)) {
        if (bohr[u].flag) {
            occ.push_back({i - patterns[bohr[u].ix].length() + 1, bohr[u].ix + 1});
        }
    }
}

static bool sortResult(const pair<int, int>& x1, const pair<int, int>& x2){
    if (x1.first < x2.first){
        return true;
    }
    else if(x1.first == x2.first){
        if(x1.second < x2.second){
            return true;
        }
        else return false;
    }
    else return false;
}

std::ostream& operator<< (std::ostream &out, vector<pair<int, int>>& result)
{
    stable_sort(result.begin(), result.end(),sortResult);
    for (const pair<int, int> &it:result) {
#ifdef JOKER
        out << it.first << endl;
#else
        out << it.first << " " << it.second << endl;
#endif
    }
    return out;
}

void findAllOccurrences(const string &s) {
    vector<pair<int, int>> occ;
    vector<pair<int, int>> res;
    int u = 0;
    for (int i = 0; i < s.length(); i++) {
        u = getAdvanceVertex(u, functionIndex(s[i]));
        follow(u, i + 1, occ);
    }
    for(pair<int, int> it:occ){
        bool b = true;
        for(int i = 0; i<patterns[it.second-1].length();i++){
            if(i+it.first-1<s.length()&&s[i+it.first-1]!=patterns[it.second-1][i]&&patterns[it.second-1][i]!=J)b=false;
        }
        if(b)res.push_back(it);
    }
    cout << res;
}

int main() {
    string S = "";
    string tmp = "";

#ifdef JOKER
    cin>>S;
    cin>>tmp;
    cin>>J;
    addString(tmp);
#else
    int n;
    cin >> S;
    cin >> n;
    while (n--) {
        cin >> tmp;
        addString(tmp);
    }
#endif
    findAllOccurrences(S);
}
