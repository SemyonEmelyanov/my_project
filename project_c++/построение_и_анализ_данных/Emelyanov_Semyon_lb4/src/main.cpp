#include <iostream>
#include <vector>
#include<time.h>
#define LOGING true
using namespace std;

std::ostream& operator<< (std::ostream &out, const vector <int> result)
{
    for (int i = 0; i < result.size() - 1;i++){
        out<<result[i]<< ",";
    }
    out<<result[result.size()-1]<<"\n";
    return out;
}

void cmpFunction1(string& str1,string& str2 ){
    vector <int> result;
    string str = str1 + " " + str2;
    vector <int> vec(str.length()+1,0);
    for (int i = 1,k = 0;i < str.length();i++, k = vec[i-1] ){
        while (k > 0 && str[i] != str[k])
        {
            k = vec[k-1];
        }
        if( str[i] == str[k] ) 
        {
            k++;
        }
        vec[i] = k;
    }

    for( int i = 2*str1.length()-1; i <vec.size() ; i++ )
    {
        if(vec[i] == str1.length())
        {
            result.push_back(i-2*str1.length());
        }
    }
    if(result.size() == 0){
        cout << -1;
        return;
    }
    cout << result;
}

void cmpFunction2(string& str1,string& str2 ){
    vector <int> result;
    string str = str2+" "+str1+str1;
    string strCOPY;
    if (str1.length() != str2.length()){
        cout<< -1;
        return;
    }
    for (int i = 1, j = 0;i < str.length();i++){
        if(str[i]==str[j]){
            j++;
            if (j == str2.length()){
                cout<< i - 2*str2.length();
                return;
            } 
        }
        else if (j > 0) j = 0;
    }
    cout << -1;
    return;
}

int main(){
    bool flag_task = false;
    string str1,str2;
    cin>>str1>>str2;
    if(flag_task) cmpFunction1(str1,str2);
    else cmpFunction2(str1,str2);
    return 0;
}