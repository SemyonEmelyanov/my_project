#include "square.h"

square::square(int x, int y, int sizeSquare){
    this->x = x;
    this->y = y;
    this->sizeSquare = sizeSquare;
}
int square::getXCoordinate(){
    return x;
}
int square::getYCoordinate(){
    return y;
}
int square::getSizeSquare(){
    return sizeSquare;
}
void square::setXCoordinate(int x){
    this->x = x;
}
void square::setYCoordinate(int y){
    this->y = y;
}
void square::setSizeSquare(int sizeSquare){
    this->sizeSquare = sizeSquare;
}