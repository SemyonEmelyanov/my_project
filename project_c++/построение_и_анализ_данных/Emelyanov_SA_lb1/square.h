#pragma once
class square{
    private:
        int x;
        int y;
        int sizeSquare;
    public:
        square(int x, int y, int sizeSquare);
        int getXCoordinate();
        int getYCoordinate();
        int getSizeSquare();
        void setXCoordinate(int x);
        void setYCoordinate(int y);
        void setSizeSquare(int sizeSquare);
};