#include "UnitTest.h"



class UnitTest{
    friend class slicingSquare;
        slicingSquare* test;
        int counterTests;
    public:
        UnitTest(int N){
            counterTests = 1;
            cout<<"\nThis test "<< counterTests << endl;
            this->test = new slicingSquare(N);
            cout<<"Test designer  - ok"<<endl;
        }
        ~UnitTest(){
            delete test;
        }
        void shiftUnitTest(int N){
            counterTests++;
            delete this->test;
            cout<<"\nThis test "<< counterTests << endl;
            this->test = new slicingSquare(N);
            
        }
        void checkPrintSquare(){
            test->printSquare();
            cout<<"Test method printSquare - ok"<<endl;
        }
        void checkCompositeNumber(int N){
            bool flag = false;
            for (int i = 3; i < N-1; i+=2){
                if (N%i == 0){
                    flag = true;
                }
            }
            if( flag == test->checkCompositeNumber(N)){
                cout<<"Test method checkCompositeNumber - ok"<<endl;
            }
            else{
                cout<<"Test method checkCompositeNumber - no"<<endl;
            }
        }
        void checkisNotCellFree(int N){
            if(test->isNotCellFree(N)[0] == -1){
                cout<<"Test method isNotCellFree - ok"<<endl;
            }
            else{
                cout<<"Test method isNotCellFree - ok"<<endl;
            }

        }
        void checkMap(int N){
            test->clearMap(0,0,(int) N/2);
            bool flag = true;
            for(int i = 0;i < (int) N/2;i++ ){
                for(int j = 0;j < (int) N/2;j++ ){
                    if (test->map[i][j] != 0){
                        flag = false;
                    }
                }
            }
            if(flag){
                cout<<"Test method clearMap - ok"<<endl;
            }
            else{
                cout<<"Test method clearMap - no"<<endl;
            }
            test->completionMap(0,0,(int) N/2);
            flag = true;
            for(int i = 0;i < (int) N/2;i++ ){
                for(int j =0;j < (int) N/2;j++ ){
                    if (test->map[i][j] != (int) N/2){
                        flag = false;
                    }
                }
            }
            if(flag){
                cout<<"Test method completionMap - ok"<<endl;
            }
            else{
                cout<<"Test method completionMap - no"<<endl;
            }

        }


};




void RunUnitTest(int N = 2){
    UnitTest test(N);
    test.checkPrintSquare();
    if(N > 2) test.checkCompositeNumber(N);
    test.checkisNotCellFree(N);
    test.checkMap(N);
}






int main(){
    int N;
    cin>>N;
    RunUnitTest(N);


    return 0;
}