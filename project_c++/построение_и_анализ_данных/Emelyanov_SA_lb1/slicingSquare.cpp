 #include "slicingSquare.h"
 slicingSquare::slicingSquare(int& N){
    limatSquer = 17;
    if(N%2 == 0){
        goodSquares.push_back(square(0,0,N/2));
        goodSquares.push_back(square(N/2,0,N/2));
        goodSquares.push_back(square(0,N/2,N/2));
        goodSquares.push_back(square(N/2,N/2,N/2));
        printSquare();
    }
    else if (checkCompositeNumber(N)){
        map = vector<vector<int>>(N, vector<int>(N,0));
        squares.push_back(square(0,0,(N/coefficient+1)/2));
        completionMap(0,0,(N/coefficient+1)/2);
        squares.push_back(square(0,(N/coefficient+1)/2,(N/coefficient-1)/2));
        completionMap(0,(N/coefficient+1)/2,(N/coefficient-1)/2);
        squares.push_back(square((N/coefficient+1)/2,0,(N/coefficient-1)/2));
        completionMap((N/coefficient+1)/2,0,(N/coefficient-1)/2);
        searchSquares(N/coefficient);
        for(square i: goodSquares){
            i.setXCoordinate(i.getXCoordinate()*coefficient);
            i.setYCoordinate(i.getYCoordinate()*coefficient);
            i.setSizeSquare(i.getSizeSquare()*coefficient);
        }
        printSquare();
    }
    else{
        map = vector<vector<int>>(N, vector<int>(N,0));
        squares.push_back(square(0,0,(N+1)/2));
        completionMap(0,0,(N+1)/2);
        squares.push_back(square(0,(N+1)/2,(N-1)/2));
        completionMap(0,(N+1)/2,(N-1)/2);
        squares.push_back(square((N+1)/2,0,(N-1)/2));
        completionMap((N+1)/2,0,(N-1)/2);
        searchSquares(N);
        printSquare();
    }
}
void slicingSquare::printSquare(){
    cout<<goodSquares.size()<<"\n";
    for (square i: this->goodSquares){
        cout<<coefficient*i.getXCoordinate()+1<<" "<<coefficient*i.getYCoordinate()+1<<" "<<coefficient*i.getSizeSquare()<<"\n";
    }
}

bool slicingSquare::checkCompositeNumber (int N){
    for (int i = 3; i < N-1; i+=2){
        if (N%i == 0){
            this->coefficient = N/i;
            return true;
        }
    }
    return false;
}

void slicingSquare::completionMap (int coordx, int coordy, int sizesq){
    for(int i = coordx;i < coordx + sizesq;i++ ){
        for(int j = coordy;j < coordy + sizesq;j++ ){
            map[i][j] = sizesq;
        }
    }
}
void slicingSquare::clearMap (int coordx, int coordy, int sizesq){
    for(int i = coordx;i < coordx + sizesq;i++ ){
        for(int j = coordy;j < coordy + sizesq;j++ ){
            map[i][j] = 0;
        }
    }
}
void slicingSquare::searchSquares(int N){
    vector <int> pos = isNotCellFree(N);
    if (pos[1] == -1){
        limatSquer = counterPrintSquares;
        goodSquares = squares;
        // printmatrix();
        return;
    }

    for (int n = 1; n <= (N+1)/2; n++){
        if( pos[0]+n > N || pos[1] + n > N || (counterPrintSquares + 1) > limatSquer){
            return;
        }

        for(int i = pos[0];i < pos[0] + n;i++ ){
            for(int j = pos[1];j < pos[1] + n;j++ ){
                if (map[i][j]!=0){
                    return;
                }
            }
        }
        completionMap(pos[0],pos[1],n);
        counterPrintSquares++;
        squares.push_back(square(pos[0],pos[1],n));
        searchSquares(N);
        squares.pop_back();
        clearMap(pos[0],pos[1],n);
        counterPrintSquares-=1;
    }

}
vector<int> slicingSquare::isNotCellFree(int sizeSq){    
    for (int i = (sizeSq-1)/2 ; i < sizeSq;i++){
        for (int j = (sizeSq-1)/2; j < sizeSq; j++){
            if(map[i][j]==0){
                return {i,j};
            }
        }
    }
    return {-1,-1};
}
void slicingSquare::printmatrix(){
    cout<<endl;
    for (vector<int>i: map ){
        for (int j : i){
            cout<<j<<" ";
        }
        cout<<endl;
    }
}