#pragma once
#include "square.h"
#include <iostream>
#include <vector>
#include<time.h>
#include "UnitTest.h"
using namespace std;
class slicingSquare{
    friend class UnitTest;
    private:
        vector<square> squares;
        vector<square> goodSquares;
        int coefficient = 1;
        vector<vector<int>> map;
        vector<int> positionFreeCell = {-1,-1};
        int goodSizeSquare = -1;
        int counterPrintSquares = 3;
        int limatSquer;
        void printSquare();
        bool checkCompositeNumber (int N);
        void completionMap (int coordx, int coordy, int sizesq);
        void clearMap (int coordx, int coordy, int sizesq);
        void searchSquares(int N);
        vector<int> isNotCellFree(int sizeSq);
        void printmatrix();
    public:
        slicingSquare(int& N);
};
