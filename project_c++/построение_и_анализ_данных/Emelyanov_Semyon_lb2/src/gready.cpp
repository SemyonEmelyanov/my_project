// #include "Library.h"
#include "gready.h"

singlton* singlton::intance = nullptr;

bool gready(set<int>&visited, vector<int>&path  ){
    singlton* ter = singlton::getIntance();
    bool flag = false;
    int vertex = path.back();
    if (vertex == ter->finish) return true;
    vector<Box> vectorVertexs(ter->link[vertex].begin(), ter->link[vertex].end());
    sort(vectorVertexs.begin(), vectorVertexs.end(), mySort);
    for (Box i:vectorVertexs){
        if (!visited.count(i.vertex)){
            visited.insert(i.vertex);
            path.push_back(i.vertex);
            if (flag |= gready(visited, path)){
                return true;
            }      
        }
        
    }
    path.pop_back();
    return false;
}

bool mySort(const Box &first, const Box &second){
    return first.heft < second.heft;
}

void print(vector<int>&path){
    for (auto i: path){
        cout<< (char)i;
    }
    cout<<endl;
    
}

Box::Box(int vertex, double heft){
    this-> vertex = vertex;
    this-> heft = heft;
}
bool Box::operator< (const Box& a) const{
    return this->vertex < a.vertex;
}