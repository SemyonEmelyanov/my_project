#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
using namespace std;



class Box;
bool mySort(const Box &first, const Box &second);
bool gready(set<int>&visited, vector<int>&path);

class Box{
    public:
        int vertex;
        double heft;
        Box(int vertex, double heft);
        bool operator< (const Box& a) const;

};

class singlton{
    private:
        static singlton* intance;
        singlton(): start(0), finish(0){
        }
    public:
        int start;
        int finish;
        map<int, set<Box>> link;
        static singlton* getIntance(){
            if (intance == nullptr){
                intance = new singlton();
            }
            return intance;
        }


};

bool gready(set<int>&visited, vector<int>&path  );
bool mySort(const Box &first, const Box &second);
void print(vector<int>&path);