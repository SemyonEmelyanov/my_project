// #include "Library.h"
#include "agready.h"


int main(){
    singlton* ter = singlton::getIntance();
    char a, b;
    double s;
    double heuristic;
    cin>>a>>b;
    ter->start = (int) a;
    ter->finish = (int) b;
    while(!cin.eof()){
        cin>>a>>b>>s>>heuristic;
        ter->link[(int)a].insert(Box((int)b, s, heuristic));
    }
    vector<int> path; 
    Agready(path);
    print(path);
    return 0;
}