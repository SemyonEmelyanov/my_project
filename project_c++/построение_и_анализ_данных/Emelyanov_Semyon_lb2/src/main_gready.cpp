// #include "Library.h"
#include "gready.h"

int main(){
    char a, b;
    singlton* ter = singlton::getIntance();
    double s;
    cin>>a>>b;
    ter->start = (int) a;
    ter->finish = (int) b;
    while(!cin.eof()){
        cin>>a>>b>>s;
        ter->link[(int)a].insert(Box((int)b, s));
    }
    set<int> visited;
    vector<int> path;
    path.push_back(ter->start);
    visited.insert(ter->start);
    gready(visited, path);
    print(path);
    return 0;
}