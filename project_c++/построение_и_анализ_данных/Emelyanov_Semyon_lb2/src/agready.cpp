// #include "Library.h"
#include "agready.h"

singlton* singlton::intance = nullptr;

static bool mySort(const Label& first, const Label& second) {
        return  first.weightFunction() < second.weightFunction();
}

void print(vector<int>&path){
    for (auto i: path){
        cout<< (char)i;
    }
    cout<<endl;
    
}

void Agready(std::vector<int>& path) {
    std::map<int, Label> labels;
    std::vector<Label> Stack;
    bool flag;
    singlton* intance = singlton::getIntance();
    int vtx = intance->start;
    labels[vtx] = Label(vtx, -1, 0, 0);
    Stack.push_back(labels[vtx]);
    while (!Stack.empty()) {
        vtx = Stack.front().name;
        if(vtx==intance->finish)break;
        Stack.erase(Stack.begin());
        flag = false;
        for (auto i:intance->link[vtx]) {
            Label lb = Label(i.vertex, vtx, labels[vtx].heft + i.heft, i.heuristic);
            if (labels.count(i.vertex)) {
                if (lb.heft < labels[i.vertex].heft) {
                    flag = true;
                    labels[i.vertex] = lb;

                    Stack.push_back(labels[i.vertex]);
                    stable_sort(Stack.begin(), Stack.end(), mySort);
                }
            } else {
                flag = true;
                labels[i.vertex] = lb;
                Stack.push_back(labels[i.vertex]);
                stable_sort(Stack.begin(), Stack.end(), mySort);
            }
        }
    }
    vtx = intance->finish;
    while (vtx != intance->start) {
        path.push_back(vtx);
        vtx = labels[vtx].prev;
    }
    path.push_back(intance->start);
    path = vector<int>(path.rbegin(), path.rend());
}


Label::Label() {
    this->name = 0;
    this->prev = 0;
    this->heft = -1;
    this->heuristic = 0;
}

Label::Label(int name, int prev, double heft, double heuristic) {
    this->name = name;
    this->prev = prev;
    this->heft = heft;
    this->heuristic = heuristic;
}

double Label::heuristicFunction() const { 
    singlton* intance = singlton::getIntance();
    return (int) abs((char) name - (char) intance->finish); 
}
double Label::weightFunction() const { 
    return heft + /*heuristicFunction()*/ heuristic; 
}

Box::Box(int vertex, double heft, double heuristic){
    this-> vertex = vertex;
    this-> heft = heft;
    this->heuristic = heuristic;
}
bool Box::operator< (const Box& a) const{
    return this->vertex < a.vertex;
}