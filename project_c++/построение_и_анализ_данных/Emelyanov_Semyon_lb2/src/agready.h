#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
using namespace std;


class Box{
    public:
        int vertex;
        double heft;
        double heuristic;
        Box(int vertex, double heft, double heuristic);
        bool operator< (const Box& a) const;
};

class singlton{
    private:
        static singlton* intance;
        singlton(): start(0), finish(0){
        }
    public:
        int start;
        int finish;
        map<int, set<Box>> link;
        static singlton* getIntance(){
            if (intance == nullptr){
                intance = new singlton();
            }
            return intance;
        }


};


class Label {
    public:
        int name;
        int prev;
        double heft;
        double heuristic;
        double heuristicFunction() const;
        double weightFunction() const ;
        Label();
        Label(int name, int prev, double heft, double heuristic);
};

void print(vector<int>&path);
void Agready(std::vector<int>& path);
static bool mySort(const Label& first, const Label& second);