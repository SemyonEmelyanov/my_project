package com.example.colormyviews

import android.graphics.Color
import android.os.Bundle
import android.view.View
//import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListeners()
    }

    private fun setListeners() {
        val boxOneText = findViewById<TextView>(R.id.box_one_text)
        val boxTwoText = findViewById<TextView>(R.id.box_two_text)
        val boxThreeText = findViewById<TextView>(R.id.box_three_text)
        val boxFourText = findViewById<TextView>(R.id.box_four_text)
        val boxFiveText = findViewById<TextView>(R.id.box_five_text)
        val rootConstraintLayout = findViewById<View>(R.id.constraint_layout)
//        val redButton = findViewById<Button>(R.id.red_button)
//        val greenButton = findViewById<Button>(R.id.green_button)
//        val yellowButton = findViewById<Button>(R.id.yellow_button)
//        val clickableViews: List<View> =
//            listOf(
//                boxOneText, boxTwoText, boxThreeText,
//                boxFourText, boxFiveText, rootConstraintLayout,
//                redButton, greenButton, yellowButton
//            )
        val clickableViews: List<View> =
            listOf(
                boxOneText, boxTwoText, boxThreeText,
                boxFourText, boxFiveText, rootConstraintLayout,
            )
//        for (item in clickableViews) {
//            item.setOnClickListener { makeColored(it,clickableViews) }
//        }
        for (item in clickableViews) {
            item.setOnClickListener { makeColored(clickableViews) }
        }
    }

//    private fun makeColored(view: View, clickableViews: List<View>) {
//        when (view.id) {
//            R.id.box_one_text -> view.setBackgroundColor(Color.DKGRAY)
//            R.id.box_two_text -> view.setBackgroundColor(Color.GRAY)
//            R.id.box_three_text -> view.setBackgroundColor(Color.BLUE)
//            R.id.box_four_text -> view.setBackgroundColor(Color.MAGENTA)
//            R.id.box_five_text -> view.setBackgroundColor(Color.BLUE)
//            R.id.red_button -> clickableViews[2].setBackgroundResource(R.color.my_red)
//            R.id.yellow_button -> clickableViews[3].setBackgroundResource(R.color.my_yellow)
//            R.id.green_button -> clickableViews[4].setBackgroundResource(R.color.my_green)
//            else -> view.setBackgroundColor(Color.LTGRAY)
//        }
//
//    }
    private fun makeColored(clickableViews: List<View>) {
        clickableViews[0].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))
        clickableViews[1].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))
        clickableViews[2].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))
        clickableViews[3].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))
        clickableViews[4].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))
        clickableViews[5].setBackgroundColor(Color.argb(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)))

    }




}