

package com.example.android.trackmysleepquality

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.android.trackmysleepquality.database.SleepDatabase
import com.example.android.trackmysleepquality.database.SleepDatabaseDao
import com.example.android.trackmysleepquality.database.SleepNight
import org.junit.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class SleepDatabaseTest {

    private lateinit var sleepDao: SleepDatabaseDao
    private lateinit var db: SleepDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, SleepDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        sleepDao = db.sleepDatabaseDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetNight() {
        val night = SleepNight()
        sleepDao.insert(night)
        val tonight = sleepDao.getTonight()
        assertEquals(tonight?.sleepQuality, -1)
    }
    @Test
    @Throws(Exception::class)
    fun updateTest(){
        val night = SleepNight()
        sleepDao.insert(SleepNight())
        sleepDao.update(night)
        val tonight = sleepDao.getTonight()
        assertEquals(tonight?.sleepQuality, -1)
    }
    @Test
    @Throws(Exception::class)
    fun getTest(){
        sleepDao.insert(SleepNight())
        val tonight  = sleepDao.get(2)
        assertEquals(tonight , null)
    }
    @Test
    @Throws(Exception::class)
    fun clearTest(){
        sleepDao.insert(SleepNight())
        sleepDao.clear()
        assertEquals(sleepDao.getTonight() , null)
    }
    @Test
    @Throws(Exception::class)
    fun getAllNightsTest(){
        sleepDao.insert(SleepNight())
        assertEquals(sleepDao.getAllNights().value ,  null)
    }
}