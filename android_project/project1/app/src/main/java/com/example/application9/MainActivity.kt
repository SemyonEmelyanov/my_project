package com.example.application9

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var diceImage : ImageView
    lateinit var diceImage2 : ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        diceImage = findViewById(R.id.dice_image)
        diceImage2 = findViewById(R.id.dice_image2)
        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener { rollDice() }
       // val countUpButton: Button = findViewById(R.id.count_up_button)
       // countUpButton.setOnClickListener { countUp() }
    }
//    private fun rollDice() {
//        val randomInt = (1..6).random()
//        //val resultText: TextView = findViewById(R.id.result_text)
//        //resultText.text = randomInt.toString()
//        //val diceImage: ImageView = findViewById(R.id.dice_image)
//        val drawableResource = when (randomInt) {
//            1 -> R.drawable.dice_1
//            2 -> R.drawable.dice_2
//            3 -> R.drawable.dice_3
//            4 -> R.drawable.dice_4
//            5 -> R.drawable.dice_5
//            else -> R.drawable.dice_6
//        }
//        diceImage.setImageResource(drawableResource)
//    }
    //private fun countUp(){
        //val resText: TextView = findViewById(R.id.result_text)
        //if (resText.text == "Hello World!") resText.text = "1"
       // else {
         //   if (resText.text.toString().toInt()< 6) {
           //     resText.text = (resText.text.toString().toInt() + 1 ).toString()
          //  }
       // }
   // }
    private fun rollDice(){
        diceImage.setImageResource(getImage())
        diceImage2.setImageResource(getImage())
    }
    private fun getImage(): Int{
        val randomInt = (1..6).random()
        val drawableResource = when (randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        return drawableResource
    }
}